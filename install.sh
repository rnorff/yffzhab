#!/bin/bash

echo "1. Copy habrecorder to /etc/init.d/habrecorder"
read -p "Press any key..."
sudo cp ./habrecorder /etc/init.d/



echo "2. Add permissions: sudo chmod 755 /etc/init.d/habrecorder"
read -p "Press any key..."
sudo chmod 755 /etc/init.d/habrecorder



echo "3. Copy gps-logger.py to /home/pi/gps-logger.py"
read -p "Press any key..."
sudo cp ./gps-logger.py /home/pi/



echo "4. Make gps-logger.py executable"
read -p "Press any key..."
sudo chmod a+x /home/pi/gps-logger.py



echo "5. Add to init.d: sudo update-rc.d habrecorder defaults"
read -p "Press any key..."
sudo update-rc.d habrecorder defaults



echo "7. Install python serial"
read -p "Press any key..."
sudo apt install python-serial -y



echo "8. Set up WiFi hotspot on the pi"

sudo apt install hostapd -y
sudo echo "interface=wlan0" >> /etc/hostapd/hostapd.conf
sudo echo "driver=nl80211" >> /etc/hostapd/hostapd.conf
sudo echo "country_code=DE" >> /etc/hostapd/hostapd.conf
sudo echo "hw_mode=g" >> /etc/hostapd/hostapd.conf
sudo echo "channel=6" >> /etc/hostapd/hostapd.conf
sudo echo "wme_enabled=1" >> /etc/hostapd/hostapd.conf
sudo echo "ieee80211n=1" >> /etc/hostapd/hostapd.conf
sudo echo "macaddr_acl=0" >> /etc/hostapd/hostapd.conf
sudo echo "auth_algs=1" >> /etc/hostapd/hostapd.conf
sudo echo "ignore_broadcast_ssid=0" >> /etc/hostapd/hostapd.conf
sudo echo "wpa=2" >> /etc/hostapd/hostapd.conf
sudo echo "wpa_key_mgmt=WPA-PSK" >> /etc/hostapd/hostapd.conf
sudo echo "wpa_pairwise=CCMP" >> /etc/hostapd/hostapd.conf
sudo echo "wpa_group_rekey=86400" >> /etc/hostapd/hostapd.conf
sudo echo "ssid=Pi_AP" >> /etc/hostapd/hostapd.conf
sudo echo "wpa_passphrase=Raspberry" >> /etc/hostapd/hostapd.conf

sudo echo "iface wlan0 inet static" >> /etc/network/interfaces
sudo echo "  address 192.168.42.1" >> /etc/network/interfaces
sudo echo "  netmask 255.255.255.0" >> /etc/network/interfaces

sudo sed -i "/^#DAEMON_CONF=/c\DAEMON_CONF=/etc/hostapd/hostapd.conf" /etc/init.d/hostapd
sudo sed -i "/^DAEMON_CONF=/c\DAEMON_CONF=/etc/hostapd/hostapd.conf" /etc/init.d/hostapd
sudo sed -i "/^#DAEMON_CONF=/c\DAEMON_CONF=\"/etc/hostapd/hostapd.conf\"" /etc/default/hostapd
sudo sed -i "/^DAEMON_CONF=/c\DAEMON_CONF=\"/etc/hostapd/hostapd.conf\"" /etc/default/hostapd

# disable dhcp daemon as we only want to run with manual ips
sudo echo "denyinterfaces wlan0" >> /etc/dhcpd.conf
sudo systemctl stop dhcpcd.service
sudo systemctl disable dhcpcd.service

# start the access point on boot (somehow hostapd throws an error so we need to unmask and enable it)
sudo systemctl unmask hostapd
sudo systemctl enable hostapd


echo "IMPORTANT: if all went well you should reboot now and verify that pictures and videos are taken and stored in /home/pi/camera and that the GPS log is written to /home/pi/gps-log.txt"
read -p "Press any key to finish..."