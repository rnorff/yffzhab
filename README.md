
# A simple HAB (high altitude balloon) project

Before starting, please enable in raspi-config:

1. Camera
2. Serial (but not shell/terminal output!)

Once enabled you should install git, python serial on your Pi:

1. run `sudo apt update && sudo apt upgrade`
2. run `sudo apt install git`
3. clone this repository
4. cd into the repository(!) and run `sudo install.sh`

## habrecorder

A simple init.d-script that records the gps coordinates and videos for some minutes and then takes some pictures in a row and repeats that for some hours. The script can be installed by *running install.sh from within the git repository directory* and rebooting your Pi!

If you ever need to stop the automatic execution of the script call: sudo /etc/init.d/habrecorder stop. This will allow you to e.g. remove recorded video files.

In order to start recording make sure that a clear.txt exists in /boot/ (`sudo touch /boot/clear.txt`). This will *delete*(!!!) all previous photos and videos and start recording new ones. 

