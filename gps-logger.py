#!/usr/bin/env python
import time
import serial
import binascii
import struct

def current_time_millis():
    return round(time.time() * 1000)

def did_confirm_navigation_config(ser, navConfig):
    print("Reading nav config response bytes")
    start_time = current_time_millis()
    expected_response = bytearray([0xB5, 0x62, 0x05, 0x01, 0x02, 0x00, navConfig[2], navConfig[3], 0, 0])

    # calculate checksum (last 2 bytes of above array)
    for i in range(2, 8):
        expected_response[8] += expected_response[i]
        expected_response[9] += expected_response[8]

    expected_response[8] = expected_response[8] & 0xFF
    expected_response[9] = expected_response[9] & 0xFF

    print("Expected response")
    print(binascii.hexlify(expected_response))

    recvdByteIndex = 0
    while True:
        if recvdByteIndex > 9:
            return True

        if current_time_millis() - start_time > 2000:
            print("Timed out confirming nav config response")
            #time.sleep(1)
	    return False

        if ser.in_waiting:
            read_bytes = ser.read(1)
            read_byte = int(binascii.hexlify(read_bytes), 16)

            print("Read byte: " + binascii.hexlify(read_bytes))

            if read_byte == expected_response[recvdByteIndex]:
                print("HIT!")
                recvdByteIndex += 1
            else:
                recvdByteIndex = 0



def send_navigation_config(ser):
    print("Sending nav config to GPS")

    navConfig = bytearray([0xB5, 0x62, 0x06, 0x24, 0x24, 0x00, 0xFF, 0xFF, 0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x10, 0x27, 0x00, 0x00,
    0x05, 0x00, 0xFA, 0x00, 0xFA, 0x00, 0x64, 0x00, 0x2C, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x16, 0xDC])

    didSetNavConfig = False
    while not didSetNavConfig:
        ser.write(navConfig)
	ser.write("\r\n")
	ser.flush()
        didSetNavConfig = did_confirm_navigation_config(ser, navConfig)


def send_test_request(ser):
    mon_ver = bytearray([0xB5, 0x62, 0x0a, 0x04, 0x00, 0x00, 0x0e, 0x34])
    ser.write(mon_ver)
    amount_read = 0

    while True:
        if ser.in_waiting:
            read_bytes = ser.read(1)
            print(">> " + binascii.hexlify(read_bytes))
            amount_read += 1

        if amount_read >= 40:
            time.sleep(10)
            print("10s wait, resending")
            ser.write(mon_ver)
            amount_read = 0

print("Starting serial")

ser = serial.Serial(
        port='/dev/serial0', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
        baudrate = 9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1
)
print("Connected")

#send_test_request(ser)

ser.write("$PUBX,41,1,0007,0003,4800,0*13\r\n")
ser.flush()
print("Wrote PUBX protocol config")

ser.baudrate = 4800
send_navigation_config(ser)
print("Sent nav config")

print("Starting to read GPS data")
rmc = ""
gll = ""
gga = ""

start_time = current_time_millis()
while True:
    line = ser.readline()
    if line.startswith("$GPRMC"):
        rmc = line
    elif line.startswith("$GPGLL"):
        gll = line
    elif line.startswith("$GPGGA"):
        gga = line
    elif "RMC" in line and not rmc:
        rmc = line
    elif "GLL" in line and not gll:
        gll = line
    elif "GGA" in line and not gga:
        gga = line

    if current_time_millis() - start_time > 3000:
        with open('/home/pi/gps-log.txt', 'a') as gps_log_file:
            gps_log_file.write(gll)
            gps_log_file.write(rmc)
            gps_log_file.write(gga)
            gll = ""
            rmc = ""
            gga = ""
            start_time = current_time_millis()
